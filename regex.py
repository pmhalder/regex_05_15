import re

# Question 1
# Given a list of English words, return the words that have
# any of these suffixes: -ate, -ize, -ify, and -able
# for example, "doable" and "illuminate" would both be matches
def findRhymes(students):
    return []

# Question 2
# Given a list of Standard addresses, extract the street name
# ex: Mickey Mouse, 5002 Mickey Drive, ToonTown, CA 88833
# Get "Mickey Drive". Note: Names can be any alpha characters, address numbers can be 2-5,
# Street names are always two words!
def findStreetNames(addresses):
    return []

# Question 3
# Validate if the code is a Pantone swatch. They come in various formats but assume the following two:
# 032 EC #three digits and 1-2 upper case letters
# 18-1663 TMX # two digits followed by a hyphen followed by four digits, then 1-3 upper case letters
# return True if a valid Pantone swatch by these standards
def pantoneSwatch(code):
    return False


# Question 4
# Given a single multiplication or division equation, return whether or not the
# expression is correct: ex. 6x6=30, return True
# assume no whitespaces. Operation will either be multiply as: * and dividing as /
def autograder(expression):
    return False


# Question 5
# Given a list of flight departure and arrivals from a very important CEO
# Sort the list by the earliest departure time
# assume the following format: LAX 07:00 MSP 10:30
# airport codes are always 3 letters, time is in 00:00
def flightSchedule(fullSchedule):
    return []


# Question 6
# Given a list of students names and grades, return the names of
# students with a grade less than 59
def findFailingStudents(students):
    return []


# Question 7
# Given a list of files, return the filenames that don't have
# a .exe or .bat extension
def filterFiles(files):
    return []


# Question 8
# Given a person's name and email, verify that the email is a valid
# firstName.lastName@pyladies.com email address
def emailVerify(contactInfo):
    return False


# Question 9
# Return Credit Card Type (or 'Invalid')
#    Visa - start with number 4. New cards have 16 digits, old cards have 13.
#    MasterCard - start with numbers 51 - 55 and have 16 digits
#    AmericanExpress - start with 34 or 37 and have 15 digits
#    Discover - start with 6011 or 65 and have 16 digits
#    Invalid - none of the above
def verifyCreditCard(creditCard):
    return ""


# Question 10
# Reformat a date in DD.MM.YYYY format to MM/DD/YY
# One date at a time!
def dateTranslation(date):
    return ""


# Question 11
# Given a roman numeral at or above the value of 1000, return True if it is a valid roman numeral
# C = 100
# M = 1000
# Only C's and M's will be used in this example 
def romanNumerals(numeral):
    return False


###### Testing Unit. Move along, nothing to see here. ######

import unittest

class TestRegex(unittest.TestCase):

    def test_q1(self):
        list = ['phosphate', 'cat,' 'create', 'dog', 'nullify', 'superb', 'customize']
        answer = ['phosphate', 'create', 'nullify', 'customize']
        self.assertEqual(findRhymes(list), answer,
                         "Expected to get %s" % ', '.join(answer))

    def test_q2(self):
        list = ['Goody Twoshoes 99', 'Average Joe 76', 'John Dummy 58', 'Amazing Amanda 103']
        answer = ["John Dummy"]
        self.assertEqual(findFailingStudents(list), answer,
                         "Expected to get [%s] not [%s]" % (', '.join(answer),
                                                            ', '.join(findFailingStudents(list))))

    def test_q3(self):
        list = ['snookie.bat', 'thatsAllFolks.exe', 'justRight_.batch', 'fun.pdf']
        answer = ['justRight_.batch', 'fun.pdf']
        self.assertEqual(filterFiles(list), answer,
                         "Expected to get %s"%', '.join(answer))


    def test_q4(self):
        QandA = [
            ("Cari Reiche cari.reiche@pyladies.com", True),
            ("Kaileen Kraemer kaileen.kreamer@pyladies.com", False),
            ("Ben Jeffrey-Gates ben.jeffrey-gates@pyladies.com", True)
        ]
        for q, a in QandA:
            self.assertEqual(emailVerify(q), a,
                             "Expected to get %r not %r" % (a, emailVerify(q)))


    def test_q5(self):
        QandA = [
            ('032 EC', True),
            ('18-1663 TMX', True),
            ('0331 C', True),
            ('19-1761 TC', True),
        ]

        for q, a in QandA:
            self.assertEqual(pantoneSwatch(q), a,
                             "Expected to get %s not %s" % (a, dateTranslation(q)))

    def test_q6(self):
        QandA = [
            ('12.06.2009', '06/12/09'),
            ('14.03.2015', '03/14/15'),
            ('09.12.2020', '12/09/20')
        ]

        for q, a in QandA:
            self.assertEqual(dateTranslation(q), a,
                             "Expected to get %s not %s" % (a, dateTranslation(q)))


    def test_q7(self):
        names = ['Brienne of Tarth, 3344 Traveling Road, Wilderness, TN  88833',
                 'Bella Swan, 405 Cactus Drive, Phoenix, AZ 88833',
                 'Tony Stark, 50 Private Drive, Stark Industries, CA 91211'
        ]

        street_names = ['Traveling Road', 'Cactus Drive', 'Private Drive']
        self.assertEqual(findRhymes(names), street_names,
                         "Expected to get %s" % ', '.join(street_names))

    def test_q8(self):
        QandA = [
            ("6011136660067159", "Discover"),
            ("342755569419091", "AmericanExpress"),
            ("4552289085142", "Visa"),
            ("5275652776147934", "MasterCard"),
            ("401328834526308", "Invalid"),
            ("377567947857003", "AmericanExpress"),
            ("4175998880276590", "Visa"),
            ("3799000621568954", "Invalid"),
            ("6557983685015518", "Discover"),
            ("5604531433462299", "Invalid"),
            ("6611394927237100", "Invalid"),
        ]
        for q, a in QandA:
            self.assertEqual(verifyCreditCard(q), a,
                             "Expected to get %s not %s" % (a, verifyCreditCard(q)))

    def test_q9(self):
        equations = [
            ('6*18=108', True),
            ('455/7=65', True),
            ('32*6=160', False),
            ('134/4=25', False)]
        for e, a in equations:
            self.assertEqual(autograder(e), a,
                             "Expected to get %s not %s" % (a, autograder(e)))

    def test_q10(self):
        schedule = [
        'LAX 07:00 MSP 10:30',
        'APN 08:00 MEM 11:50',
        'SMX 06:30 LAX 07:30',
        'STS 02:20 ORD 05:10',
        'BUF 05:30 CMI 07:00',
        'ATL 03:00 JFK 04:30',
        'BNA 01:10 JLN 03:05',
        'LAX 02:00 PDX 05:05',
        'STC 04:45 ONT 07:55',
        'SNA 04:00 LAX 04:45',
        ]
        sorted_schedule = [
        'BNA 01:10 JLN 03:05',
        'LAX 02:00 PDX 05:05',
        'STS 02:20 ORD 05:10',
        'ATL 03:00 JFK 04:30',
        'SNA 04:00 LAX 04:45',
        'STC 04:45 ONT 07:55',
        'BUF 05:30 CMI 07:00',
        'SMX 06:30 LAX 07:30',
        'LAX 07:00 MSP 10:30',
        'APN 08:00 MEM 11:50',
        ]
        self.assertEqual(flightSchedule(schedule), sorted_schedule,
                         "Expected to get %s" % ', '.join(sorted_schedule))

    def test_q11(self):
        QandA = [
            ('M', True),
            ('MM', True),
            ('MMM', True),
            ('MMMM', False),
            ('MD', True),
            ('MCM', True),
            ('MMMCCC', True),
            ('MCMC', False),

        ]
        for q, a in QandA:
            self.assertEqual(dateTranslation(q), a,
                             "Expected to get %s not %s" % (a, dateTranslation(q)))


def main():
    unittest.main()


if __name__ == '__main__':
    main()